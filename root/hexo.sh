#!/usr/bin/with-contenv bash
HOME=/config

export NVM_DIR="/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
nvm use 10.15.3

cd /code
npm install
npm install -g hexo-cli
hexo generate
hexo server -p 4000 -i 0.0.0.0