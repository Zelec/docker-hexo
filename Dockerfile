# Docker Hexo image for auto generation & building
# Created by Isaac Towns <Zelec@Timeguard.ca>

# Uses basic ubuntu:bionic image to pull nvm and install node 10.15.3
FROM ubuntu:bionic AS nvm
ENV NVM_DIR=/nvm
RUN \
    apt-get update && \
    apt-get install -y curl git && \
    mkdir "$NVM_DIR" && \
    git clone https://github.com/nvm-sh/nvm.git "$NVM_DIR" && \
    cd "$NVM_DIR" && \
    git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1)` && \
     \. "$NVM_DIR/nvm.sh" && \
    nvm install 10.15.3

# Uses lsiobase instead of the node image to help with UID and GID changes.
# Uses NVM to bypass permission issues due to node trying to use the wrong folders for data.
FROM lsiobase/ubuntu:latest AS docker-hexo
LABEL maintainer="Isaac Towns <Zelec@TimeGuard.ca>"
COPY --from=nvm /nvm /nvm
# Installs build-essential for npm compilation needs.
RUN \
    apt-get update && \
    apt-get install -y build-essential && \
    apt-get clean -y && \
    rm -rf /tmp/* /var/lib/apt/lists/* /var/tmp/*
ADD ./root /