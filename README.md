# Docker Hexo Dev  
Docker image for Hexo Development

Mount a hexo website to /code and this container will auto generate the website at [localhost:4000](http://localhost:4000)

This Docker image should be only used for development, not for production use, You have been warned.

Since this container uses [Linuxserver.io's](https://linuxserver.io) ubuntu base image, the following environment variables are available to help with generation

* TZ (America/New_York)
* PUID (1000)
* PGID (1000)

## Quickstart
* Clone this repository `git clone https://gitlab.com/Zelec/docker-hexo.git` & cd into the `docker-hexo` directory
* Type in the terminal `echo -n "PUID:" && id -u "$USER" && echo -n "PGID:" && id -g "$USER"` to get your user & group id's
* Edit the `docker-compose.yml` to fix the PUID and PGID environment variables to what you need & to change the host directory for `/code`
* Type into the terminal `docker-compose up`
* You can now edit files in the hexo directory and those changes should be auto generated by the end software.

If you make some major changes to the image (Changing the theme for example) you may need to type `docker-compose restart` to have the container fully regenerate the website with `hexo generate`